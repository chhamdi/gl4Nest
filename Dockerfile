FROM node:12-alpine

RUN /bin/sh -c "apk add --no-cache bash"

RUN apk update && apk --no-cache add --virtual builds-deps build-base python

COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install

# Add your source files
COPY . .

EXPOSE 3000

CMD npm start