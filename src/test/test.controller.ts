import { Controller, Delete, Get, Post, Put } from '@nestjs/common';

@Controller('test')
export class TestController {

  @Get('')
  get(): string {
    return 'Get Test';
  }

  @Post('')
  post(): string {
    return 'Post Test';
  }

  @Put('')
  put(): string {
    return 'PUT Test';
  }

  @Delete('')
  delete(): string {
    return 'Delete Test';
  }
  
}
