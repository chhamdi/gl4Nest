import { UpperCasePipe } from './../pipes/uppercase.pipe';
import { Body, Controller, Post } from '@nestjs/common';

@Controller('skills')
export class SkillsController {
  @Post()
  setToUppercase(@Body(UpperCasePipe) body: string): string {
    return body
  }
}
