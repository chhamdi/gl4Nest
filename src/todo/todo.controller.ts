import { TodoService } from './todo.service';
import { UpdateTodoDto } from './dto/update-todd.dto';
import { CreateTodoDto } from './dto/create-todo.dto';
import { Todo } from './models/todo.model';
import { Controller, Get, Post, Body, Param, Delete, Put, NotFoundException } from '@nestjs/common';

@Controller('todo')
export class TodoController {

  constructor(private todoService: TodoService) { }

  @Get('')
  getTodos() {
    return this.todoService.findTodos();
  }

  @Get(':id')
  getTodoById(@Param('id') id: string) {
    return this.todoService.findTodoById(id);
  } 
  
  @Post('')
  addTodo(@Body() body: CreateTodoDto) {
    return this.todoService.addTodo(body);
  }

  @Delete(':id')
  deleteTodoById(@Param('id') id: string) {
    return this.todoService.deleteTodoById(id);
  } 

  @Put(':id')
  updateTodoById(@Param('id') id: string, @Body() body: UpdateTodoDto) {
    return this.todoService.updateTodoById(id, body);
  } 

}
