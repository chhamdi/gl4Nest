import { UpdateTodoDto } from './dto/update-todd.dto';
import { CreateTodoDto } from './dto/create-todo.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Todo } from './models/todo.model';

@Injectable()
export class TodoService {

  private todos: Todo[] = [];

  constructor() {
  }

  findTodos(): Todo[] {
    return this.todos;
  }

  findTodoById(id: string) {
    const todo = this.todos.find( todo => todo.id == id);
    if(!todo)
      throw new NotFoundException(`todo ${id} not found`);
    return todo;
  } 
  
  addTodo(todo: CreateTodoDto): Todo {
    const newTodo = new Todo();
    newTodo.name = todo['name'];
    newTodo.description = todo['description'];
    this.todos.push(newTodo);
    return newTodo;
  }

  deleteTodoById(id: string): Todo {
    const todo = this.findTodoById(id);
    this.todos.splice(this.todos.indexOf(todo), 1);
    return todo;
  } 

  updateTodoById(id: string, newTodo: UpdateTodoDto):  Todo {
    const todo = this.todos.filter( todo => todo.id == id);
    if(!todo.length)
      throw new NotFoundException(`todo ${id} not found`);
    const { name, description, status } = newTodo;
    todo[0].name = name? name : todo[0].name;
    todo[0].description = description? description : todo[0].description;
    todo[0].status = status? status : todo[0].status ;
    return todo[0];
  } 

}
