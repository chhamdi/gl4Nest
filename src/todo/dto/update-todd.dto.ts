import { TodoStatusEnum } from './../enums/todo-status.enum';
import { IsIn, IsNotEmpty, IsOptional, IsString, MinLength, MaxLength } from 'class-validator';

export class UpdateTodoDto {

  @IsNotEmpty()
  @MinLength(3, {
    message: "$property min length: $constraint1"
    }
  )
  @MaxLength(10, {
    message: "$property max length: $constraint1"
    }
  )
  @IsOptional()
  name: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  @MinLength(10, {
    message: "$property min length: $constraint1"
  })
  description: string;

  @IsIn([
    TodoStatusEnum.waiting,
    TodoStatusEnum.done,
    TodoStatusEnum.actif,
  ], {
    message: "Le status est invalide"
  })
  @IsOptional()
  status: TodoStatusEnum;
  
}