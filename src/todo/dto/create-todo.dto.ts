import { IsNotEmpty, IsString, MinLength, MaxLength } from 'class-validator';

export class CreateTodoDto {

  @IsNotEmpty()
  @MinLength(3, {
      message: "$property min length: $constraint1"
    }
  )
  @MaxLength(10, {
    message: "$property max length: $constraint1"
  }
)
  name: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(10, {
    message: "$property min length: $constraint1"
  })
  description: string;

}