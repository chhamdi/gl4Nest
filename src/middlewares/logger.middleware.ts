import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    console.log('From Logger Middleware body is:', req.body);
    console.log('From Logger Middleware Request ip is:', req.connection.remoteAdress);
    next();
  }
}
