import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class UpperCasePipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if(!value.skills)
      throw new BadRequestException('bad request');
    if(metadata.type == 'body') {
      let data = value.skills;
      let s = '-';
      data.forEach(el => {
        s+=`${el}-`
      });
      return s;
    }
      

  }
}
